#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This code implements various equations of state. The pressure is in Pa.
"""
# %%
import numpy as np
from scipy.optimize import root

import utils

R = 8.3144626
params = {
    "ch4": [4.599e6, 190.56, 0.011],  # Poling et al. (2001)
}

# ----- Basic operations ----- #


def check_gas(s):
    species = params.keys()

    import inspect

    methodName = inspect.stack()[1][3]
    if s.lower() not in species:
        raise ValueError(f"{methodName}: No applicable gas!")


# ----- EoS ----- #


def dmw(P, T, s="ch4"):
    """
    Calculate the compressibility factor using the  Duan-Moller-Weare equation of state for CH4, CO2, H2S, NH3, C2H6 and water.
    """

    P, T = utils.check(P, T)

    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()
    Pc, Tc, _ = params[s]

    Pr = P / Pc
    Tr = T / Tc
    Vr = np.zeros_like(Pr)

    # EoS parameters
    # CH4
    a = np.array(
        [
            8.72553928e-2,
            -0.752599476,
            0.375419887,
            1.07291342e-2,
            5.4962636e-3,
            -1.84772802e-2,
            3.18993183e-4,
            2.11079375e-4,
            2.01682801e-5,
            -1.65606189e-5,
            1.19614546e-4,
            -1.08087289e-4,
            4.48262295e-2,
            0.75397,
            7.7167e-2,
        ]
    )

    # function of Z

    def fz(Vr, Tr):
        Zf = (
            1
            + Vr ** np.array([[-1, -2, -4, -5]])
            @ np.reshape(a[:12], [3, 4], order="F").T
            @ Tr ** np.array([[0, -2, -3]]).T
            + a[12] / Tr**3 / Vr**2 * (a[13] + a[14] / Vr**2) * np.exp(-a[14] / Vr**2)
        )
        return Zf.ravel()

    for i, (Tri, Pri) in enumerate(zip(Tr, Pr)):

        def fdmw(V, Tri=Tri, Pri=Pri):
            return fz(V, Tri) * Tri - V * Pri

        Vr[i] = root(fdmw, 1.0, method="hybr").x

    Z = Pr * Vr / Tr
    Z[P == 0] = 1

    return Z.reshape(shape)
