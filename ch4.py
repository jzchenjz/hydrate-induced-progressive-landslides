#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate equilibrium properties in methane gas-liquid-hydrate system, with possible presence of salt, based on Duan and Mao (2006), Henry et al. (1990), Sun and Duan (2005) and Duan and Sun (2006).
"""
# %%
import numpy as np
from scipy.optimize import root

import eos
import hydrate
import unit
import utils
import water

Tq = 272.9  # invariant Q1
Mw = 0.018015  # water molar mass
R = 8.3144626  # universal gas constant
Pc = 4.599e6  # critical pressure of CH4
Tc = 190.56  # critical temperature of CH4
H = 54e3  # dissociation heat
# Table 3
Cμ = np.array(
    [
        8.3143711,
        -7.2772168e-4,
        2148.9858,
        -1.4019672e-5,
        -667434.49,
        7.698589e-3,
        -5.0253331e-6,
        -3.0092013,
        484.68502,
        0,
    ]
)
Cλ = np.array(
    [-0.81222036, 1.0635172e-3, 188.94036, 0, 0, 4.4105635e-5, 0, 0, 0, -4.6797718e-11]
)
Cξ = np.insert(np.zeros([1, 9]), 0, -2.9903571e-3)

# ----- Gas-liquid equilibrium ----- #


def pv(P, T, ms=0):
    """
    Calculate the partial molar volume of CH4 using eq. 10 of Duan and Mao (2006). P is in Pa, and ms is in mol/kg. The partial molar volume in pure water is slightly larger than that in other works (e.g., O'Sullivan and Smith (1970)). The result is in m³/RT.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    P = P / 1e5  # convert to bar

    pmv = (
        Cμ[5]
        + Cμ[6] * T
        + Cμ[7] / T
        + Cμ[8] / T**2
        + 2 * ms * (Cλ[5] + 2 * Cλ[9] * P * T)
    )
    return pmv / 1e5  # convert to m³/RT


def phi(P, T):
    """
    Calculate the fugacity coefficient lnφ for CH4 using eq. A4 of Duan and Mao (2006) and the DMW eos of methane. P is in Pa.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)
    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()

    Pr = P / Pc
    Tr = T / Tc

    Z = eos.dmw(P, T, "ch4")
    Vr = Z * Tr / Pr

    # parameters for CH4, from Table 3 of Duan et al. (1992)
    a = np.array(
        [
            8.72553928e-2,
            -0.752599476,
            0.375419887,
            1.07291342e-2,
            5.4962636e-3,
            -1.84772802e-2,
            3.18993183e-4,
            2.11079375e-4,
            2.01682801e-5,
            -1.65606189e-5,
            1.19614546e-4,
            -1.08087289e-4,
            4.48262295e-2,
            0.75397,
            7.7167e-2,
        ]
    )

    # eq. A4
    def fphi(Zx, Vx, Tx):
        return (
            Zx
            - 1
            - np.log(Zx)
            + 1
            / np.array([[1, 2, 4, 5]])
            * Vx ** np.array([[-1, -2, -4, -5]])
            @ np.reshape(a[:12], [3, 4], order="F").T
            @ Tx ** np.array([[0, -2, -3]]).T
            + a[12]
            / 2
            / Tx**3
            / a[14]
            * (a[13] + 1 - (a[13] + 1 + a[14] / Vx**2) * np.exp(-a[14] / Vx**2))
        )

    lnφ = np.array(list(map(fphi, Z, Vr, Tr))).flatten()
    return lnφ.reshape(shape)


def par(C, P, T):
    """
    Calculate the parameterization with coefficients C and pressure P and T. P is in bar, and T is in K.
    """
    # eq. 9
    return (
        C[0]
        + C[1] * T
        + C[2] / T
        + C[3] * T**2
        + C[4] / T**2
        + C[5] * P
        + C[6] * P * T
        + C[7] * P / T
        + C[8] * P / T**2
        + C[9] * P**2 * T
    )


def potential(P, T):
    """
    Calculate the standard chemical potential μ of CH4 in liquid, in hypothetically ideal solution of unit molality using Duan and Mao (2006) eq. 9. P is in Pa, and the result is in μ/RT.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    P = P / 1e5  # convert to bar

    # in μ_l0 / R / T
    return par(Cμ, P, T)


def interaction(P, T):
    """
    Calculate the interaction parameters λ_CH4_Na and ξ_CH4_Na_Cl, using Duan and Mao (2006) eq. 9. P is in Pa.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    P = P / 1e5  # convert to bar

    return par(Cλ, P, T), par(Cξ, P, T)


def mf(P, T, ms=0):
    """
    Calculate the mole fraction of CH4 in the gas, using Duan and Mao (2006) eq. 4 and eq. 5. P is in Pa, and ms is the molality of NaCl in the liquid.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    Pw = water.psat(T)
    ΔP = P - Pw
    mvw = water.vm(P, T)
    φw = water.phi(P, T)
    x = unit.molal2mf(ms)

    return 1 - (1 - 2 * x) * Pw / φw / P * np.exp(mvw * ΔP / R / T)


def vlsol(P, T, ms=0):
    """
    Calculate the V-L solubility of CH4 in water under P and T. P is in Pa, ms is the molality of NaCl in mol/kg, and the solubility is in mol/kg.
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    if np.any(T < Tq):
        print("vlsol: Temperature is below the lower quadruple point!")

    y = mf(P, T, ms)
    y[T < Tq] = 0  # in ice, no dissolved gas
    lnφ = phi(P, T)
    μ = potential(P, T)
    λ, ξ = interaction(P, T)

    P = P / 1e5  # P in bar
    # eq. 15
    return y * P * np.exp(lnφ - μ - 2 * λ * ms - ξ * ms**2)


# ----- van der Waals-Platteeuw ----- #


def equilibrium(P, T, ms):
    """
    Solve for the equilibrium temperature and pressure of CH4 hydrate with salt molality ms.
    """

    nwater = 23  # 46 water molecules
    ν1 = 1 / nwater  # 2 small cavities
    ν2 = 3 / nwater  # 6 large cavities
    C1, C2 = langmuir(T)
    f = P * np.exp(phi(P, T))
    θS, θL, *_ = hydrate.occupancy(C1, C2, f)
    dμ = hydrate.potential(P, T, Tq) / R / T

    mg = vlsol(P, T, ms)
    λ, ξ = interaction(P, T)

    return (
        dμ
        - hydrate.activity(T, λ, ξ, ms, mg)
        + ν1 * np.log(1 - θS)
        + ν2 * np.log(1 - θL)
    )


def temperature(Pf, ms=0):
    """
    Calculate the dissociation temperature of CH4 hydrate under given temperature Tf and salt molality ms. With high NaCl molality, it is difficult to find a good trial guess.
    """

    if isinstance(Pf, (int, float, list, np.ndarray)):
        Pf = np.array([Pf], dtype=float).flatten()

    if np.any(Pf < 1e5):
        raise ValueError("temperature: Pressure is too low!")

    # trial value range from Table 14 of Duan and Mao (2006)
    T0 = 293
    aw = 1
    Tt = T0 / (1 - R * T0 * np.log(aw) / H)

    Tf = np.zeros_like(Pf)
    for i, Pi in enumerate(Pf):
        Tf[i] = root(lambda Tx, P=Pi: equilibrium(P, Tx, ms), Tt).x

    return Tf


# ----- Langmuir constants ----- #


def langmuir(T):
    """
    Calculate the temperature-dependent Langumir constants using the method in Sun and Duan (2007) Appendix A.
    """
    if isinstance(T, (int, float, list)):
        T = np.array([T], dtype=float).flatten()

    A1 = -24.027993
    B1 = 3134.7529
    A2 = -22.683049
    B2 = 3080.3857
    return np.exp(A1 + B1 / T), np.exp(A2 + B2 / T)


# %%
