#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A collection of useful functions.
"""
import numpy as np


def check(P, T):
    """
    Check if the pressure and temperature have compatible sizes.
    """

    # check P, T dimensions and verify consistence
    if isinstance(P, (int, float, list, np.ndarray)):
        P = np.array([P], dtype=float).flatten()
    if isinstance(T, (int, float, list, np.ndarray)):
        T = np.array([T], dtype=float).flatten()

    if len(P) == 1:
        P = np.tile(P, np.shape(T))
    elif len(T) == 1:
        T = np.tile(T, np.shape(P))
    elif len(P) != len(T):
        raise ValueError("check: P and T must have same dimensions or be singular!")

    return P, T
