#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate thermodynamic properties of CH4 based on the Setzmann & Wagner (1991) model. All thermodynamic properties are calculated from the dimensionless Helmholtz energy
Φ = A / (RT) where A(ρ, T) = h_0(T) - R * T - T * s_0(ρ, T)
and its derivatives with respect to P and T.
Adapted from the SUGAR Toolbox (Kossel, Bigalke, Piñerom Haeckel, 2013).
"""
# %%
import numpy as np
from scipy.optimize import root


ρc = 162.66  # critical density [kg/m³]
Tc = 190.564  # critical temperature [K]
R = 8.3144626  # gas constant [J/mol/K]
M = 0.016042  # methane molar mass [kg/mol]


def density(P, T):
    """
    Calculate the density of CH4 gas at P (in Pa) and T (in K). The range is 0.012 MPa < P < 1000 MPa, 90 K < T < 620 K.
    """

    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()
    ρ = np.zeros_like(P)

    Pind = np.logical_or(P < 12e3, P > 1e9)
    Tind = np.logical_or(T < 90, T > 620)
    if np.any(np.logical_or(Pind, Tind)):
        if np.any(Pind):
            P[Pind] = np.nan
        if np.any(Tind):
            T[Tind] = np.nan
        print("density: Input values out of range!")
        print("Equations valid for 0-1 GPa, 90-620 K.")
        ρ[np.logical_or(Pind, Tind)] = np.nan

    # start with ideal gas density
    ρ0 = P * M / R / T

    # Check if arrays for P and T contain NaN. If yes, also set corresponding density values to NaN. Calculate density for allowed P and T only. If the fit function returns EXITFLAG<=0, the iteration process has terminated with a non-satisfying result. In this case, the initial value is changed and the fit procedure is repeated.

    for i, (Pi, Ti) in enumerate(zip(P, T)):
        if not np.isnan(ρ0[i]):
            ρ[i] = root(
                lambda x, Pi=Pi, Ti=Ti: x - rhofunc(x, Pi, Ti), ρ0[i], method="hybr"
            ).x

        # if the initial value is too far from the solution
        # EXITFLAG = -1  # initialize EXITFLAG value
        # k = 0  # set counter to 0
        # ρnew = ρ0[i]  # first initial value for density
        # while (EXITFLAG < 1) and (not np.isnan(ρnew)):
        #     k += 1
        #     ρ[i], _, EXITFLAG, *_ = fsolve(lambda x: x - rhofunc(x, Pi, Ti),
        #                                    ρnew,
        #                                    full_output=True)
        #     # change initial value for fit
        #     ρnew = ρ0[i] + k * (-1)**k

        #     if ρ[i] <= 0:
        #         EXITFLAG = -1  # reject negative densities

        #     if ρnew <= 0:
        #         EXITFLAG = 10  # terminate the while loop
    return ρ.reshape(shape)


def Phir(δ, τ):
    """
    Calculate the residual of the Helmholtz energy Φr and its derivatives with respect to the reduced density dΦrdδ.
    """

    # calculation of sum terms with parameters ni, di, ti
    ni = np.array(
        [
            0.4367901028e-1,
            0.6709236199,
            -0.1765577859e1,
            0.8582330241,
            -0.1206513052e1,
            0.5106513052,
            -0.4000010791e-3,
            -0.1247842423e-1,
            0.3100269701e-1,
            0.1754748522e-2,
            -0.3171921605e-5,
            -0.2240346840e-5,
            0.2947056156e-6,
        ]
    )
    di = np.array([1, 1, 1, 2, 2, 2, 2, 3, 4, 4, 8, 9, 10])
    ti = np.array([-0.5, 0.5, 1, 0.5, 1, 1.5, 4.5, 0, 1, 3, 1, 3, 3])

    nj = np.array(
        [
            0.1830487909,
            0.1511883679,
            -0.4289363877,
            0.6894002446e-1,
            -0.1408313996e-1,
            -0.3063054830e-1,
            -0.2969906708e-1,
            -0.1932040831e-1,
            -0.1105739959,
            0.9952548995e-1,
            0.8548437825e-2,
            -0.6150555662e-1,
            -0.4291792423e-1,
            -0.1813207290e-1,
            0.3445904760e-1,
            -0.2385919450e-2,
            -0.1159094939e-1,
            0.6641693602e-1,
            -0.2371549590e-1,
            -0.3961624905e-1,
            -0.1387292044e-1,
            0.3389489599e-1,
            -0.2927378753e-2,
        ]
    )
    dj = np.array([1, 1, 1, 2, 4, 5, 6, 1, 2, 3, 4, 4, 3, 5, 5, 8, 2, 3, 4, 4, 4, 5, 6])
    tj = np.array(
        [0, 1, 2, 0, 0, 2, 2, 5, 5, 5, 2, 4, 12, 8, 10, 10, 10, 14, 12, 18, 22, 18, 14]
    )
    cj = np.array([1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4])

    nk = np.array([0.9324799946e-4, -0.6287171518e1, 0.1271069467e2, -0.6423953466e1])
    dk = np.array([2, 1, 1, 1])
    tk = np.array([2, 0, 1, 2])
    ak = np.array([20, 40, 40, 40])
    bk = np.array([200, 250, 250, 250])
    ck = np.array([1.07, 1.11, 1.11, 1.11])
    ek = 1

    return (
        np.sum(ni * di * δ ** (di - 1) * τ**ti)
        + np.sum(nj * δ ** (dj - 1) * τ**tj * np.exp(-(δ**cj)) * (dj - cj * δ**cj))
        + np.sum(
            nk
            * δ**dk
            * τ**tk
            * np.exp(-ak * (δ - ek) ** 2 - bk * (τ - ck) ** 2)
            * (dk / δ - 2 * ak * (δ - ek))
        )
    )


def rhofunc(ρ, P, T):
    """
    Function to calculate density at given pressure based on the equation
    ρ = P * M / (R * T * (1 + ρ/ρc * d(Φr)/d(ρ/ρc)))
    """

    δ = ρ / ρc
    τ = Tc / T
    # calculate d(Φr)/d(ρ/ρc)
    dΦrdδ = Phir(δ, τ)

    # calculate density
    return P * M / (R * T * (1 + δ * dΦrdδ))
