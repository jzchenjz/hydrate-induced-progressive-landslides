#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Commands used to convert units. If the isarray flag is true, the input is treated as a separated array, i.e., not in the same solution.
"""

import numpy as np

Mw = 0.018015  # water molar mass

# ----- Concentration ----- #


def molal2mf(m, isarray=False):
    """
    Calculate the mole fractions of solute from the molalities.
    """

    m = np.array(m, dtype=float)

    return m * Mw / (1 + m * Mw) if isarray else m * Mw / (1 + np.sum(m) * Mw)
