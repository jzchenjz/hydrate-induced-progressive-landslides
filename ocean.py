#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate the temperature, pressure, salinity, and methane
"""
# %%
import numpy as np

R = 8.3144626
M = 0.016042
M_NaCl = 0.0584428
p0 = 101325

# ----- Seawater density ----- #


def brine(P, T, ms=0):
    """
    Calculate the density of water with salt using the Spivey et al. (2004) model. P is in Pa, T is in K, and ms is brine salinity in mol/kg.
    """
    # Table 2
    Dw = np.array([-0.127213, 0.645486, 1.03265, -0.070291, 0.639589])
    Ew = np.array([4.221, -3.478, 6.221, 0.5182, -0.4405])
    Fw = np.array([-11.403, 29.932, 27.952, 0.20684, 0.3768])
    # Table 3
    Dcm1 = np.array([-0.0076402, 0.036963, 0.0436083, -0.333661, 1.185685])
    Dcm1_2 = np.array([0.0003746, -0.0003328, -0.0003346, 0, 0])
    Dcm3_2 = np.array([0.0010998, -0.0028755, -0.0035819, -0.72877, 1.92016])
    Dcm2 = np.array([-0.00007925, -0.00000193, -0.00034254, 0, 0])
    # Table 4
    Ecm = np.array([0, 0, 0.1353, 0, 0])
    Fcm3_2 = np.array([-1.409, -0.361, -0.2532, 0, 9.216])
    Fcm1 = np.array([0, 5.614, 4.6782, -0.307, 2.6069])
    Fcm1_2 = np.array([-0.1127, 0.2047, -0.0452, 0, 0])

    # eq. 3, expression for coefficients
    t = (T - 273.15) / 100
    t2 = t**2

    def a(C, t):
        return (C[0] * t2 + C[1] * t + C[2]) / (C[3] * t2 + C[4] * t + 1)

    # water and brine density at reference 70 MP
    p = P / 7e7
    # eq. 9 and 10
    ρb0 = (
        a(Dw, t)
        + a(Dcm1, t) * ms
        + a(Dcm2, t) * ms**2
        + a(Dcm1_2, t) * ms ** (1 / 2)
        + a(Dcm3_2, t) * ms ** (3 / 2)
    )
    # eq. 12 to 14
    Eb = a(Ew, t) + a(Ecm, t) * ms
    Fb = (
        a(Fw, t)
        + a(Fcm3_2, t) * ms ** (3 / 2)
        + a(Fcm1, t) * ms
        + a(Fcm1_2, t) * ms ** (1 / 2)
    )
    Ib = 1 / Eb * np.log(Eb * p + Fb)
    Ib0 = 1 / Eb * np.log(Eb + Fb)

    # brine density
    return ρb0 * np.exp(Ib - Ib0) * 1000  # convert to kg/m³
