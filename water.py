#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This class contains methods to calculate the pure water properties, such as viscosity and saturation pressure of water vapor.
"""
import numpy as np

import utils

Pc = 22.064e6  # critical pressure of water vapor
Tc = 647.14  # critical temperature of water vapor
pa = 101325  # atmospheric pressure
T0 = 273.15  # freezing point
Mw = 0.018015  # water molar mass


def psat(T):
    """
    Calculate the saturation pressure of water vapor using Wagner and Pruss (1993). This does not deviate from the Antoine equation with A = 5.11564, B = 1687.537, C = 230.17.
    """

    if isinstance(T, (int, float, list)):
        T = np.array([T]).flatten()

    # critical temperature and pressure of water
    Tr = T / Tc
    t = 1 - Tr
    # Wagner and Pruss (1993) eq. 1
    a1 = -7.85951783
    a2 = 1.84408259
    a3 = -11.7866497
    a4 = 22.6807411
    a5 = -15.9618719
    a6 = 1.80122502

    return Pc * np.exp(
        1
        / Tr
        * (a1 * t + a2 * t**1.5 + a3 * t**3 + a4 * t**3.5 + a5 * t**4 + a6 * t**7.5)
    )


def vm(P, T):
    """
    Calculate the molar volume of liquid water, using the density of saturated water according to Klauda and Sandler (2000).
    The result is slightly smaller than the fit using Wagner and Pruss (1993).
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    # eq. 21
    a1 = -10.9241
    a2 = 2.5e-4
    a3 = -3.532e-4
    a4 = 1.559e-7

    t = T - T0
    p = (P - pa) / 1e6

    return np.exp(a1 + a2 * t + a3 * p + a4 * p**2)


def phi(P, T):
    """
    Calculate the fugacity coefficient φ of water using eq. 6 of Duan and Mao (2006).
    """

    # check P, T dimensions and verify consistence
    P, T = utils.check(P, T)

    P = P / 1e5  # convert to bar

    # Table 1
    a1 = -1.42006707e-2
    a2 = 1.0836991e-2
    a3 = -1.5921316e-6
    a4 = -1.10804676e-5
    a5 = -3.14287155
    a6 = 1.06338095e-3

    return np.exp(a1 + a2 * P + a3 * P**2 + a4 * P * T + a5 * P / T + a6 * P**2 / T)
