# Hydrate-induced progressive landslides 



This repository contains python scripts to simulate the triggering of progressive landslides for the publication *Assessing progressive mechanical instability of submarine slopes caused by methane hydrate dissociation*. The main functions are in `stability.py`, with other auxiliary functions in `ch4.py`, `eos.py`, `hydrate.py`, `inhibitor.py`, `ocean.py`, `setzmann_ch4.py`,  `unit.py`, `utils.py` and `water.py`.

