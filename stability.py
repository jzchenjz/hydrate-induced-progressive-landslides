#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
We simulate the effect of hydrate dissociation on the stability of the marine sediment layer, with the parameters of the Shenhu hydrate site and other global sites.
"""
# %%
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy.random import lognormal, uniform
from scipy.stats import gmean

import ch4
import ocean
import setzmann_ch4

g = 9.8
x = 0.134  # methane mass fraction in hydrate
M = 0.016042  # methane molar mass
R = 8.3144626  # universal gas constant
ΔH = 54.44e3  # heat of formation of hydrate
ρh = 929  # hydrate density
ρl = 1029  # seawater density
ρs = 2720  # sediment grain density
γl = ρl * g  # unit weight of seawater
γs = ρs * g  # unit weight of dry soil
ψ = 10  # dilation angle ψ °
ν = 0.45  # Poisson's ratio
Dc = 50e-6  # characteristic slip length m
E = 70e6  # Young's modulus Pa
E_range = [10e6, 100e6]
# shenhu: A struct containing geotechnical and geological parameters for
# the Shenhu area, specifically for the GMGS3-W19 site, as studied in
# Sun et al. (2017). Other sites comply to the same structure.
#
# Fields:
#   - phi_p: Peak friction angle (φₚ) in degrees (°). Represents the
#            maximum shear strength of the sediment material.
#   - phi: Porosity (φ) of the sediment, dimensionless. This value is
#          a depth average for the overburden layer.
#   - H: Depth of the sliding surface in meters (m). A 2-element vector
#        representing the range of depths. For Shenhu, the difference is
#        a maximum hydrate layer thickness of 17.6 m.
#   - gammap: Submerged unit weight γ'=γ-γₗ (N/m³). A 2-element vector
#             representing the range of values.
#   - beta: Slope angle (β) in degrees (°). A 2-element vector
#           representing the minimum and maximum slope angles at the site.
#   - z: Water depth in meters (m). Represents the depth of the water
#        column above the site.
#   - Sg0: Initial gas saturation (Sg₀), dimensionless. Represents the
#          gas saturation at the initial state.
#   - Sh0: Initial hydrate saturation (Sh₀), dimensionless. Represents
#          the gas hydrate saturation at the initial state.
shenhu = {
    "phi_p": np.array([25]),
    "phi": np.array([0.483]),
    "H": np.array([137.95, 155.55]),
    "gammap": np.array([8.57e3]),
    "beta": np.array([2, 4]),
    "z": np.array([1273.8]),
    "Sg0": np.array([0.194]),
    "Sh0": np.array([0.452]),
}


# ----- Overpressurization ----- #
def expansion(P=5e6, T=298):
    """
    Calculate the volume expansion factor according to Xu and Germanovich (2006).
    """
    # or use the Peng-Robinson EoS instead of the Setzmann-Wagner EoS
    ρg = setzmann_ch4.density(P, T)
    ρl = ocean.brine(P, T)
    return (1 - x) * ρh / ρl + x * ρh / ρg - 1


def compression(Sg=0.2, Sl=0.8, P=5e6):
    """
    Calculate the effective compression factor according to Xu and Germanovich (2006).
    """
    shape = None
    if isinstance(P, np.ndarray):
        shape = np.shape(P)
        P = np.array([P], dtype=float).flatten()
    Te = ch4.temperature(P)

    Sg = np.array([Sg]) if isinstance(Sg, (int, float)) else Sg
    Sl = np.array([Sl]) if isinstance(Sl, (int, float)) else Sl

    dTedP = Te**2 * R / P / ΔH

    def ρg(P, T):  # methane gas density
        return setzmann_ch4.density(P, T)

    def ρl(P, T):  # water density
        return ocean.brine(P, T)

    dP, dT = 1, 1
    ρg_P = (ρg(P + dP, Te) - ρg(P - dP, Te)) / (2 * dP)
    ρg_T = (ρg(P, Te + dT) - ρg(P, Te - dT)) / (2 * dT)
    ρl_P = (ρl(P + dP, Te) - ρl(P - dP, Te)) / (2 * dP)
    ρl_T = (ρl(P, Te + dT) - ρl(P, Te - dT)) / (2 * dT)
    ρg0 = ρg(P, Te)
    ρl0 = ρl(P, Te)
    κg = (ρg_P + dTedP * ρg_T) / ρg0
    κl = (ρl_P + dTedP * ρl_T) / ρl0
    κ = κg * Sg + κl * Sl
    if shape is not None:
        κ = κ.reshape(shape)
        κg = κg.reshape(shape)
    return κ, κg


def excess(P=30e6, Sh0=0.2, Sg0=0, Sh_end=0.19):
    """
    Calculate the excess pore pressure during expassion of the gas phase in confined pore space.
    """

    P = np.array([P]) if isinstance(P, (int, float)) else P
    Shx = np.linspace(Sh0, Sh_end, 100)
    ΔSh = Shx[1] - Shx[0]
    Sgx = np.zeros((len(P), len(Shx)))
    Sgx[:, 0] = Sg0
    Px = np.zeros_like(Sgx)
    Px[:, 0] = P
    T = ch4.temperature(Px[:, 0])  # constant temperature

    for i in range(1, len(Shx)):
        Sg = Sgx[:, i - 1]
        Sl = 1 - Sg - Shx[i - 1]

        κ, κg = compression(Sg, Sl, Px[:, i - 1])
        Rv = expansion(Px[:, i - 1], T)
        Δu = -Rv * ΔSh / κ
        Px[:, i] = Px[:, i - 1] + Δu
        ρg = setzmann_ch4.density(Px[:, i], T)
        Sgx[:, i] = Sg - x * ρh / ρg * ΔSh - Sg * κg * Δu

    Pex = Px - P.reshape(-1, 1)
    return Pex, Shx, Sgx


def calculate_excess_pressure(Sh0, Sg0, P_min=5, P_max=50, ΔSh=0.01):
    """
    Calculate Pex and Pex_approx for given Sh0 and Sg0.

    Parameters:
        Sh0 (float): Initial hydrate saturation.
        Sg0 (float): Initial gas saturation.
        P_min (float): Minimum pressure in MPa (default: 5 MPa).
        P_max (float): Maximum pressure in MPa (default: 50 MPa).
        ΔSh (float): Hydrate dissociation (default: 0.01, or 1%).

    Returns:
        SS (ndarray): Meshgrid of -ΔSh values.
        PP (ndarray): Meshgrid of pressure values.
        Pex (ndarray): Exact excess pressure.
        Pex_approx (ndarray): Approximated excess pressure.
    """
    # Define the pressure range
    P = np.arange(P_min, P_max + 1) * 1e6  # Convert to Pascals

    # Calculate excess pressure
    Pex, Shx, _ = excess(P, Sh0, Sg0, Sh0 - ΔSh)
    SS, PP = np.meshgrid(Sh0 - Shx, P)

    # Small ΔSₕ approximation
    κ, _ = compression(Sg0, 1 - Sh0 - Sg0, P)
    κ = np.tile(κ.reshape([len(κ), 1]), (1, len(Shx)))
    Rv = expansion(P, ch4.temperature(P))
    Rv = np.tile(Rv.reshape([len(Rv), 1]), (1, len(Shx)))
    Pex_approx = Rv * SS / κ

    return SS, PP, Pex, Pex_approx


def plot_excess_pressure(case=None):
    """
    Plot the contour diagram of Δu for a given case.

    Parameters:
        case (dict): A dictionary containing the case parameters:
            - Sh0 (float): Initial hydrate saturation.
            - Sg0 (float): Initial gas saturation.
            - hide_ylabels (bool): If True, hide y-labels and y-tick labels.
            - panel_label (str): Label for the panel (e.g., "(a)", "(b)").
            - ax (matplotlib.axes.Axes): The subplot axis to plot on.
    """
    # Set default case parameters if not specified
    if case is None:
        case = {
            "Sh0": 0.2,
            "Sg0": 0,
            "hide_ylabels": False,
            "panel_label": "",
            "ax": plt.subplot(111),
        }
    # Extract case parameters
    Sh0 = case["Sh0"]
    Sg0 = case["Sg0"]
    hide_ylabels = case["hide_ylabels"]
    panel_label = case["panel_label"]
    ax = case["ax"]

    # Calculate Pex and Pex_approx
    SS, PP, Pex, Pex_approx = calculate_excess_pressure(Sh0, Sg0)

    # Contour levels
    levels = [0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50]

    # Plot the exact excess pressure
    cs1 = ax.contour(SS, PP / 1e6, Pex / 1e6, levels, cmap="copper", linewidths=1)
    # Plot the approximated excess pressure
    cs2 = ax.contour(
        SS,
        PP / 1e6,
        Pex_approx / 1e6,
        levels,
        cmap="copper",
        linestyles="dashdot",
        linewidths=0.5,
    )
    # Add labels to the contours
    ax.clabel(cs1, levels, fontsize=16, fmt="%.1f")
    v = [x for x in levels if x > 2]
    ax.clabel(cs2, v, fontsize=16, colors="gray", fmt="%.1f")

    # Customize the plot
    ax.set_xlim([1e-4, 1e-2])
    ax.set_ylim([5, 50])
    ax.set_xscale("log")
    ax.set_xlabel(r"$\Delta S_h$", fontsize=24)

    # Hide y-labels and y-tick labels if specified
    if hide_ylabels:
        ax.set_ylabel("")  # Hide y-label
        ax.set_yticklabels([])  # Hide y-tick labels
    else:
        ax.set_ylabel(
            r"$P$" + "\n(MPa)", rotation="horizontal", fontsize=24, labelpad=28
        )

    # Add panel label (e.g., "(a)", "(b)")
    if panel_label:
        ax.text(
            0.7,  # x position (relative to axes)
            0.95,  # y position (relative to axes)
            panel_label,
            transform=ax.transAxes,
            fontsize=22,
            va="top",
            ha="left",
        )

    # Reduce the font size of tick labels
    ax.tick_params(axis="both", which="major", labelsize=18)

    # Hide ticks on the right and top
    ax.tick_params(axis="both", which="both", top=False, right=False)

    # Set title
    if Sg0 == 0:
        ax.set_title(
            rf"$\Delta u$ (MPa) with $S_{{h}}^0$ = {Sh0*100}%",
            fontsize=24,
        )
    else:
        ax.set_title(
            rf"$\Delta u$ (MPa) with $S_{{h}}^0$ = {Sh0*100:.1f}% and $S_{{g}}^0$ = {Sg0*100:1f}%",
            fontsize=24,
        )


def diagram_excess():
    """
    Plot two contour diagrams of Δu side by side:
    - Left plot: Sh0 = 0.2, Sg0 = 0 (labeled "(a)")
    - Right plot: Sh0 = 0.4, Sg0 = 0 (labeled "(b)")
    """
    # Create a figure with two subplots side by side
    fig, axes = plt.subplots(1, 2, figsize=(18, 8))

    # Define the cases to plot
    cases = [
        {
            "Sh0": 0.2,
            "Sg0": 0,
            "hide_ylabels": False,
            "panel_label": "(a)",
            "ax": axes[0],
        },
        {
            "Sh0": 0.4,
            "Sg0": 0,
            "hide_ylabels": True,
            "panel_label": "(b)",
            "ax": axes[1],
        },
    ]

    # Plot each case
    for case in cases:
        plot_excess_pressure(case)

    # Save the figure
    plt.tight_layout()
    # plt.savefig("excess_comparison.pdf", bbox_inches="tight")
    plt.show()


# ----- Mechanics ----- #


def weight(φ):
    """
    Calculate the submerged unit weight γ'=γ-γₗ from the unit weight of saturated soil γ using the porosity φ.
    """

    return (γs - γl) * (1 - φ)


def cohesion(Sh):
    """
    Calculate the effective cohesion of sediments dependent on the hydrate saturation Sh using correlation by Lijith et al. (2019). The effective cohesion in Pa is estimated using the lower bound so that the cohesion is negligible for Sh=0.
    """
    Sh = Sh * 100
    return (0.01 + 0.004 * Sh**1.25) * 1e6


# ----- Critical stress ----- #


def infinite(site=shenhu, H=None, β=None, z=None, φp=None, Sh0=None, Sg0=None, γp=None):
    """
    Calculate the critical excess pore pressure Δu_is and corresponding hydrate dissociation amount according to the infinite slope model.
    site: A dict containing all necessary parameters (e.g., shenhu, ursa, etc.)
    """
    if H is None:
        H = min(site["H"])  # Default depth of the sliding surface
    if β is None:
        β = np.mean(site["beta"])  # Default slope angle
    if z is None:
        z = site["z"]  # Default water depth
    if φp is None:
        φp = max(site["phi_p"])  # Default peak friction angle
    if Sh0 is None:
        Sh0 = site["Sh0"]  # Default hydrate saturation
    if Sg0 is None:
        Sg0 = site["Sg0"]  # Default gas saturation
    if γp is None:
        γp = np.mean(site["gammap"])  # Default unit weight

    # Calculate cohesion
    c = cohesion(Sh0)

    # Calculate critical excess pore pressure
    σ0 = γp * H * np.cos(np.radians(β)) ** 2
    Δu_is = σ0 * (1 - np.tan(np.radians(β)) / np.tan(np.radians(φp))) + c / np.tan(
        np.radians(φp)
    )

    # Pressure at the base of the hydrate layer
    P = γl * (z + H) + γp * H

    # Small ΔSₕ approximation for confined pores
    kappa = compression(Sg0, 1 - Sh0 - Sg0, P)
    Rv = expansion(P, ch4.temperature(P))
    ΔSh = Δu_is * kappa / Rv

    return Δu_is, ΔSh


def factor(site=shenhu, Δu=None, H=None, β=None, φp=None, Sh0=None, γp=None):
    """
    Calculate the factor of safety with an excess pore pressure Δu.
    site: A dict containing all necessary parameters (e.g., shenhu, ursa, etc.)
    """
    if Δu is None:
        Δu = 1e6  # Default excess pore pressure: 1 MPa
    if H is None:
        H = min(site["H"])  # Default depth of the sliding surface
    if β is None:
        β = np.mean(site["beta"])  # Default slope angle
    if φp is None:
        φp = max(site["phi_p"])  # Default peak friction angle
    if Sh0 is None:
        Sh0 = site["Sh0"]  # Default hydrate saturation
    if γp is None:
        γp = np.mean(site["gammap"])  # Default unit weight

    # Calculate cohesion
    c = cohesion(Sh0)

    # Calculate factor of safety
    σ0 = γp * H * np.cos(np.radians(β)) ** 2
    f = np.tan(np.radians(φp))
    Fs = (c + f * (σ0 - Δu)) / γp / H / np.sin(np.radians(β)) / np.cos(np.radians(β))

    return Fs


def size(site, H=None, φp=None, β=None, E=E, γp=None):
    """
    Calculate the range of χ* for a given slope angle β.
    site: A struct containing all necessary parameters (e.g., shenhu, ursa, etc.)
    """
    if H is None:
        H = min(site["H"])  # Default depth of the sliding surface
    if φp is None:
        φp = max(site["phi_p"])  # Default peak friction angle
    if β is None:
        β = np.mean(site["beta"])  # Default slope angle
    if γp is None:
        γp = np.mean(site["gammap"])  # Default unit weight

    σ0 = γp * H * np.cos(np.radians(β)) ** 2
    φr = φp - 0.8 * ψ
    Δf = np.tan(np.radians(φp)) - np.tan(np.radians(φr))
    λ0 = 0.579
    G = E / (2 * (1 + ν))
    χ_ast = G * λ0 / (σ0 * Δf)

    return χ_ast


def slip(site, H=None, φp=None, β=None, E=None, a=None, γp=None):
    """
    Calculate the critical excess pore pressure Δu_ps to initiate a progressive failure.
    site: A struct containing all necessary parameters (e.g., shenhu, ursa, etc.)
    """
    if H is None:
        H = min(site["H"])  # Default depth of the sliding surface
    if φp is None:
        φp = max(site["phi_p"])  # Default peak friction angle
    if β is None:
        β = np.mean(site["beta"])  # Default slope angle
    if E is None:
        E = E  # Default value for E
    if a is None:
        a = 1  # Default value for a
    if γp is None:
        γp = np.mean(site["gammap"])  # Default unit weight

    χ = a / Dc

    χ_ast = size(site, H, φp, β, E, γp)
    Δu_ps = γp * H * np.cos(np.radians(β)) ** 2 * (1 - χ_ast / χ)
    Δu_ps[Δu_ps < 0] = np.nan  # Negative values when χ < χ*

    return Δu_ps


# ----- Shenhu stability tests ----- #


# %%
def mesh(N, Hrange, arange=[0.1, 1]):
    """
    Generate uniformly spaced mesh of H and χ/χ* within the given range.
    """
    H = np.linspace(Hrange[0], Hrange[1], N)  # sliding surface depth
    a = np.linspace(arange[0], arange[1], N)  # rupture size
    H, a = np.meshgrid(H, a)
    return H, a


def sample(var, N, method="uniform"):
    """
    Generate random lognormal or uniform samples based on var. The default method is 'uniform'.
    For the lognormal method, if var is a single value x0, a coefficient of variation of cv=0.05 is assumed, and the standard deviation is σ=√ln(1+cv^2), and the mean is μ=ln x0 - σ^2/2, with a range limit [exp(μ-2σ), exp(μ+2σ)].
    If var is a two-element array [x1, x2], the mean and standard deviation can be calculated from
    exp(μ-2σ) = x1 and exp(μ+2σ)=x2
    so that 95% of the distribution lies within the range [x1, x2].
    If the method is 'uniform', the samples are uniformly distributed between [x1, x2]. If a single value x0 is provide, the samples are
    uniformly distributed within the range [x0-δ, x0+δ], where δ is chosen so that with the same cv=0.05, 95% of the distribution lies within [x0-δ, x0+δ].
    """
    cv = 0.05
    if method == "lognormal":
        if np.size(var) > 1:
            μ = np.log(gmean(var))
            σ = (μ - np.log(var[0])) / 2
        else:
            σ = np.sqrt(np.log(1 + cv**2))
            μ = np.log(var) - σ**2 / 2
        limits = np.array([np.exp(μ - 2 * σ), np.exp(μ + 2 * σ)])
        return lognormal(mean=μ, sigma=σ, size=N), limits
    elif method == "uniform":
        if np.size(var) > 1:
            limits = np.asarray(var)
        else:
            δ = var * cv * np.sqrt(3)
            limits = np.array([var - δ, var + δ])
        return uniform(low=limits[0], high=limits[1], size=N), limits


def diagram_mechanical(site=shenhu, z=None, E=E, β=None):
    """
    Calculate the mechanical stability with parameters of H and ν with fixed Young's modulus E and slope angle β.
    """
    if z is None:
        z = site["z"]  # Default water depth
    if β is None:
        β = [5, 10, 15]
    if isinstance(β, (int, float)):
        β = [β]

    N = 100
    _, Hrange = sample(site["H"], N, "uniform")
    H, a = mesh(N, Hrange)
    φp = np.mean(site["phi_p"])
    γp = np.mean(site["gammap"])

    Sh0 = site["Sh0"]  # Default value for Sh0

    # Check if hydrate exists
    hydrate = Sh0 > 0

    # Pressure at the sliding surface
    P = γl * (z + H) + γp * H

    # Skip kappa and Rv calculations if Sh0 or Sg0 are missing
    if hydrate:
        Sg0 = site["Sg0"]  # Default value for Sg0
        # Small ΔSₕ approximation for confined pores
        Pc, ic = np.unique(P, return_inverse=True)
        kappa, _ = compression(Sg0, 1 - Sh0 - Sg0, Pc)
        kappa = np.reshape(kappa[ic], (N, N))
        Rv = expansion(Pc, ch4.temperature(Pc))
        Rv = np.reshape(Rv[ic], (N, N))

    Δu_ps = []
    Fs = []
    ΔSh_approx = []

    for i in range(len(β)):
        Δu = slip(site, H, φp, β[i], E, a, γp)
        Δu[Δu < 0] = np.nan
        Δu_ps.append(Δu)
        Fs.append(factor(site, Δu, H, β[i], φp, Sh0, γp))

        # Skip ΔSh_approx calculation if Sh0 or Sg0 are missing
        if hydrate:
            ΔSh_approx.append(Δu * kappa / Rv)

    # Plotting (example, customize as needed)
    styles = ["-", "--", ":", "-."]
    colors = ["k", "b", "r", "g"]

    if hydrate:
        fig = plt.figure(tight_layout=True, figsize=(12, 10))
        gs = gridspec.GridSpec(2, 4, figure=fig)
    else:
        fig = plt.figure(tight_layout=True, figsize=(12, 6))
        gs = gridspec.GridSpec(1, 4, figure=fig)

    ytickloc = [0.1, 0.2, 0.5, 1]
    yticklabel = ["0.1", "0.2", "0.5", "1"]
    # Δu_ps distribution with H and χ/χ*
    ax = fig.add_subplot(gs[0, :2])
    for i in range(len(β)):
        C = ax.contour(
            H,
            a,
            Δu_ps[i] / 1e6,
            linestyles=styles[i],
            colors=colors[i],
            linewidths=2,
        )
        ax.clabel(C, inline=True, fontsize=12)
        plt.plot([], [], styles[i], color=colors[i], label=rf"$\beta$={β[i]}°")
    ax.text(
        0.88,  # x position (relative to axes)
        0.95,  # y position (relative to axes)
        "(a)",
        transform=ax.transAxes,
        fontsize=20,
        va="top",
        ha="left",
    )
    plt.legend(loc="best", frameon=True)
    # Hide ticks on the right and top
    ax.tick_params(axis="both", which="both", top=False, right=False)
    ax.set_yscale("log")
    plt.yticks(ytickloc, labels=yticklabel)
    ax.yaxis.set_minor_formatter(plt.NullFormatter())  # Hide minor tick labels
    ax.tick_params(axis="y", which="minor", left=True)  # Keep minor ticks visible
    ax.set_title(r"$\Delta u_\mathrm{ps}$ (MPa)")
    ax.set_xlabel(r"$H$ (m)")
    ax.set_ylabel(r"$a$" + "\n(m)", rotation="horizontal")

    # Fs distribution with H and χ/χ*
    ax = fig.add_subplot(gs[0, 2:])
    for i in range(len(β)):
        C = ax.contour(
            H, a, Fs[i], linestyles=styles[i], colors=colors[i], linewidths=2
        )
        ax.clabel(C, inline=True, fontsize=12)
        plt.plot([], [], styles[i], color=colors[i], label=rf"$\beta$={β[i]}°")
    ax.text(
        0.85,  # x position (relative to axes)
        0.95,  # y position (relative to axes)
        "(b)",
        transform=ax.transAxes,
        fontsize=20,
        va="top",
        ha="left",
    )
    plt.legend(loc="best", frameon=True)
    # Hide ticks on the right and top
    ax.tick_params(axis="both", which="both", top=False, right=False)
    ax.set_yscale("log")
    plt.yticks(ytickloc, labels=yticklabel)
    ax.yaxis.set_minor_formatter(plt.NullFormatter())  # Hide minor tick labels
    ax.tick_params(axis="y", which="minor", left=True)  # Keep minor ticks visible
    ax.set_title(r"$F_S$")
    ax.set_xlabel(r"$H$ (m)")

    # ΔSh distribution with H and χ/χ* (only if Sh0 and Sg0 exist)
    if hydrate:
        ax = fig.add_subplot(gs[1, 1:3])
        for i in range(len(β)):
            C = ax.contour(
                H,
                a,
                ΔSh_approx[i] * 100,
                linestyles=styles[i],
                colors=colors[i],
                linewidths=2,
            )
            ax.clabel(C, inline=True, fontsize=12)
            plt.plot([], [], styles[i], color=colors[i], label=rf"$\beta$={β[i]}°")
        ax.text(
            0.88,  # x position (relative to axes)
            0.95,  # y position (relative to axes)
            "(c)",
            transform=ax.transAxes,
            fontsize=20,
            va="top",
            ha="left",
        )
        plt.legend(loc="best", frameon=True)
        # Hide ticks on the right and top
        ax.tick_params(axis="both", which="both", top=False, right=False)
        ax.set_yscale("log")
        plt.yticks(ytickloc, labels=yticklabel)
        ax.yaxis.set_minor_formatter(plt.NullFormatter())  # Hide minor tick labels
        ax.tick_params(axis="y", which="minor", left=True)  # Keep minor ticks visible
        ax.set_title(r"$\Delta S_h$ (%)")
        ax.set_xlabel(r"$H$ (m)")
        ax.set_ylabel(r"$a$" + "\n(m)", rotation="horizontal")

    plt.show()
    # plt.savefig("mechanical.pdf", bbox_inches="tight")


# %%
# ----- Global landslides ----- #
def read_landslides():
    """
    Read the landslide data.
    """
    file = "landslides.csv"
    df = pd.read_csv(file, index_col=0)
    parameters = {}
    for col in df.columns:
        region = col.split(" ")[0].lower()
        parameters[region] = {"name": col}  # adding region name explicitly
        for param, value in df[col].items():
            if not pd.isnull(value):
                # remove _min suffix
                param = param.replace("_min", "")
                parameters[region][param] = np.asarray([value])
                # check for _max parameter
                if "_max" in param:
                    base_param = param.replace("_max", "")
                    parameters[region][base_param] = np.append(
                        parameters[region][base_param], value
                    )
                    parameters[region].pop(param)
    return parameters


# %%
def probability(site=shenhu, FS_critical=1):
    """
    Calculate the probability of FS_ast>=FS_critical (1 or 1.2) for a submarine landslide site.
    site: A struct containing all necessary parameters (e.g., shenhu, ursa, etc.)
    """
    N = 100000
    Hs = sample(site["H"], N, "uniform")[0]
    φps = sample(site["phi_p"], N, "uniform")[0]
    βs = sample(site["beta"], N, "uniform")[0]
    Es = sample(E_range, N, "uniform")[0]
    As = sample([0.1, 1], N, "uniform")[0]
    γps = sample(site["gammap"], N, "uniform")[0]
    Sh0 = site["Sh0"]

    Δus = slip(site, Hs, φps, βs, Es, As, γps)
    FSs = factor(site, Δus, Hs, βs, φps, Sh0, γps)
    FSs = FSs[~np.isnan(FSs)]
    p = np.mean(FSs > FS_critical)

    return p, FSs


def diagram_probability(FS_critical=1):
    # Calculate the probability of FS_ast>=FS_critical (1 or 1.2) for different submarine landslide sites.

    params = read_landslides()
    sites = ["shenhu", "ursa", "beaufort", "storegga", "hinlopen", "israeli", "goleta"]
    ps = np.zeros(len(sites))
    FSs = [None] * len(sites)
    hydrate = [None] * len(sites)

    for i, s in enumerate(sites):
        site = params[s]
        ps[i], FSs[i] = probability(site, FS_critical)
        hydrate[i] = site["Sh0"] > 0

    idx_p_sorted = np.argsort(ps)
    plt.figure(figsize=(8, 6))
    gray_shades = ["0.5", "1", "0.5", "1", "0.5", "1", "0.5", "1", "0.5"]

    for i, s in enumerate(sites):
        plt.axhspan(i - 0.5, i + 0.5, facecolor=gray_shades[i], alpha=0.3)
        si = np.where(idx_p_sorted == i)[0][0]
        box = plt.boxplot(
            FSs[i],
            positions=[si],
            tick_labels=[
                params[s]["name"] if not hydrate[i] else params[s]["name"] + r"$^\ast$"
            ],
            vert=False,
            showfliers=False,
        )
        plt.setp(
            box["medians"], color="green", linewidth=2
        )  # Make the median line thicker

    plt.xscale("log")
    plt.xlabel(r"$F_S$", fontsize=20)
    FSmax = 100
    plt.xlim(right=FSmax + 10)
    # add a vertical line at FS_ast=FS_critical
    plt.axvline(x=FS_critical, color="r", linewidth=1.5, linestyle="--")

    ax = plt.gca()
    ax.tick_params(axis="y", which="both", left=False, right=False)

    # print p[i] value to the right of the boxplot, with 2 decimal precision
    for i, s in enumerate(sites):
        plt.text(
            FSmax + 50, i, f"{ps[idx_p_sorted[i]]*100:.1f}%", va="center", fontsize=15
        )
    plt.text(FSmax + 20, 6.6, r"$P(F_S\geq1)$", va="center", fontsize=15)

    # plt.show()
    plt.savefig("probability.pdf", bbox_inches="tight")


# %%
